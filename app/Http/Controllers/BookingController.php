<?php

namespace App\Http\Controllers;

use App\Booking;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Show the home page of the application
     *
     * @return Response
     */
    public function index()
    {
        return view('pages.index');
    }
    
    /**
     * Submit the booking form and save data.
     *
     * @param $request
     * @return Response
     */
    public function createBooking(Request $request)
    {
      
        $booking = new Booking;
        $booking->name = $request->name;
        $booking->day = date("Y-m-d", strtotime($request->day));
        $booking->time = $request->time;
        $booking->message = $request->message;
        if (!$booking->save()) {
          return response()->json([
              'status' => 'error',
              'message' => 'Something went wrong. Please try again.'
          ]);
        } else {
          return response()->json([
              'status' => 'success',
              'message' => 'Thankyour for booking with us. Success message here.'
          ]);
        }
    }
    
    /**
     * Load the bookings .
     *
     * @return Response
     */
    public function loadBookings()
    {
      $bookingsRecords = Booking::get();

      if (!$bookingsRecords->isEmpty()) {
        $i = 0;
        $j = 0;
        foreach ($bookingsRecords as $booking) {
          $i++;      
          $bookings[$j]['bookings'][] = [
            'id' => $booking->id,
            'ref' => $i,
            'name' => $booking->name,
            'date' => $booking->date,
            'time' => $booking->time,
            'message' => $booking->message,
          ];

          if (($i % 2) == 0) {
            $j++;
          }
        }
      } else {
        $bookings = false;
      }
      
      return response()->json([
          'bookings' => $bookings
      ]);
    }
}

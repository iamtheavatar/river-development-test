### River Development Test

- To run locally, clonse master from this repository.

- Create a MySQL database locally, copy `.env.example` to `.env` and add database connection details

- `composer update`

- `npm install`

- `php artisan migrate`

- `npm run dev`

- `chmod 777 storage -R`

Document root folder to serve from is `/public`.

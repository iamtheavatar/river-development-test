require('bootstrap')

window.Vue = require('vue');
window.axios = require('axios');

// Import libraries
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/en'

// Get components
import BookingForm from './components/BookingForm.vue';
import BookingResults from './components/BookingResults.vue';

Vue.use(ElementUI, { locale })
Vue.prototype.$http = window.axios;

// Add components
Vue.component('booking-form', BookingForm);
Vue.component('booking-results', BookingResults);

const app = new Vue({
    el: '#app',
});

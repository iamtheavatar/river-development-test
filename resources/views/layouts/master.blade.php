<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  @hasSection ('meta_keywords')
    <meta name="keywords" content="@yield('meta_keywords')">
  @endif
  @hasSection ('meta_description')
    <meta name="description" content="@yield('meta_description')">
  @endif
  <title>@yield('title')</title>
  <link href="{{ mix('css/app.css') }}" rel="stylesheet">
  <link rel="shortcut icon" href="/img/favicon.png" type="image/png">
  <link rel="canonical" href="{{ url(Request::url()) }}">
  @stack('head')
</head>

<body>
  @yield('content')

  <script src="{{ mix('js/app.js') }}"></script>
  @stack('footer')
</body>
</html>

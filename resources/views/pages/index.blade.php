@extends('layouts.master')
@section('title', 'River - Booking Dev Test')
@section('meta_keywords', '')
@section('meta_description', '')

@section('content')

@include('partials/header')

<section class="main-content" id="app">
  <div class="container">
    <div class="row">
      <div class="main-content__bar-left col-12 col-sm-6">
        <small>01</small>
        <h3>Heading</h3>
        <p class="regular bar-left mb-5">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
        <a class="button" href="#BookingForm">Book Now</a>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-6">
        <img src="/img/image-water.png" alt="RIVER" title="RIVER" class="mb-4" />
      </div>
      <div class="main-content__bar-left col-12 col-sm-6">
        <small>02</small>
        <p class="large bar-left">Lorem ipsum dolor sit amet, consetetur.</p>
      </div>
    </div>
    <div class="row">
      <div class="main-content__bar-left col-12 col-sm-6 mb-4">
        <small>03</small>
        <h3 class="bar-left">Heading</h3>
        <p class="large">Lorem ipsum dolor sit amet, consetetur.</p>
        <p class="regular">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua, vero eos et accusam et justo duo dolores et ea rebum.</p>
      </div>
      <div class="main-content__booking-form col-12 col-sm-6" id="BookingForm">
        <booking-form ref="BookingForm"></booking-form>
      </div>
    </div>
    <booking-results ref="BookingResults"></booking-results>
  </div>
</section>

@include('partials/footer')

@endsection
